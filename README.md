# laravel-requests-docs-annotation README
Quick generate comment of lrc api docs.

My new project use laravel 10 with php8.1, But occur a problem when we try to import Swagger API docs.
<https://github.com/mpociot/laravel-apidoc-generator>
This respository has not maintain for long time. And we found another project.
<https://github.com/rakutentech/laravel-request-docs>
But I felt edit comment block is annoying. So I make my own vscode extensions for lrd.
This's code is refer to auto generate comment of swagger API.
<https://github.com/qvtec/swagger-php-annotation>
## Features

- Completion snippet after /** in a Controller.php file
- use @ to add a new param


- ref <https://github.com/rakutentech/laravel-request-docs>
- ref <https://github.com/qvtec/swagger-php-annotation>

