import * as vscode from 'vscode';

export default class Completions implements vscode.CompletionItemProvider
{
    private type: string = 'lrd';

    constructor() {
        const config: any = vscode.workspace.getConfiguration('completions');
        this.type = config.get('type');
    }

    public provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>> {
        
        let result: any = [];

        if(!vscode.window.activeTextEditor) {
            return [];
        }

        // /** */
        if (document.getWordRangeAtPosition(position, /\/\*\*/) !== undefined) {
            let block = new vscode.CompletionItem("/**", vscode.CompletionItemKind.Snippet);
            block.detail = "Lrd Annotation";
            block.documentation = "Generate Lrd Annotation."
            let range = document.getWordRangeAtPosition(position, /\/\*\* \*\//);
            block.range = range;
            block.insertText = this.autoDocument(document, position);
            result.push(block);
            return result;
        }

        const lineText: string = document.lineAt(position.line).text;
        if(!(/^\s+\*/.test(lineText))) {
            return [];
        }

        // HTTP Method [* @]
        if (document.getWordRangeAtPosition(position, /\*\s\@/) !== undefined) {
            this.tags.httpMethod.forEach(tag => {
                let item = new vscode.CompletionItem(tag.tag, vscode.CompletionItemKind.Snippet);

                let snippet: string = this.getAnnotation("${1:name}", tag.snippet);
                snippet = snippet.replace(/^(?!(\s\*|\/\*))/gm, "* $1");
                snippet = snippet.replace(/^\*\s\@/g, "");

                item.insertText = new vscode.SnippetString(snippet);
                item.commitCharacters = ['@']
                item.documentation = 'Press `@` to get Lrd-php template';
            })
            
            return result;
        }

        // Lrd param [*\s\s+@]
        if (document.getWordRangeAtPosition(position, /\*\s+\@/) !== undefined) {
            this.tags.types.forEach(tag => {
                let item = new vscode.CompletionItem(tag.tag, vscode.CompletionItemKind.Snippet);

                const regexp: RegExp = new RegExp('^(\\s+\\*\\s)(\\s*)');
                const match: any = regexp.exec(lineText);
                let indent: string = match[2];

                let snippet: string = tag.snippet;
                snippet = snippet.replace(/^(?!(\s\*))/gm, "* ###$1");
                snippet = snippet.replace(/###/gm, indent);
                snippet = snippet.replace(/^\*\s+\@/g, "");

                item.insertText = new vscode.SnippetString(snippet);
                item.commitCharacters = ['@'];
                item.documentation = 'Press `@` to get Lrd-php template';
                result.push(item);
            })

            return result;
        }

        return result;
    }

    private autoDocument(document: vscode.TextDocument, position: vscode.Position): vscode.SnippetString
    {
        let templateArray = [];
        let match: any;

        // action
        let actionName: string = "";
        const nextLinePosition = new vscode.Position(position.line + 1, position.character - 3);
        if ((match = document.getWordRangeAtPosition(nextLinePosition, /public function (\w+)/)) !== undefined) {
            actionName = document.getText(match).replace('public function ', '');
        }

        // class
        let className: string = "";
        for (let n =0; n < position.line; n++) {
            const nowPosition = new vscode.Position(n, 0);
            if ((match = document.getWordRangeAtPosition(nowPosition, /class (\w+)/)) !== undefined) {
                className = document.getText(match).replace('class ', '').replace('Controller', '');
                break;
            }
        }

        let snippet: string = this.getAnnotation(className, actionName);
        templateArray.push(snippet);
        
        if(templateArray[templateArray.length -1] === ""){
            templateArray.pop();
        }

        let templateString: string = templateArray.join("\n");
        templateString = "/**\n" + templateString + "\n */";

        templateString = templateString.replace(/^$/gm, " *");
        templateString = templateString.replace(/^(?!(\s\*|\/\*))/gm, " * $1");
        // templateString = templateString.replace()
        return new vscode.SnippetString(templateString);

    }
    private getAnnotation(className: string, actionName: string): string
    {
        const path = className.split(/(?=[A-Z])/).join('-').toLowerCase();
        let snippet: string = '';
        switch (actionName) {
            case 'index':
                // Get
                snippet = ' @lrd:start\n' +
                    '  group={"${1:' + className + '}"},\n' +
                    '  summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
                break;
            case 'store':
                // Post
                snippet = ' @lrd:start\n' +
                    '   group={"${1:' + className + '}"},\n' +
                    '   summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
                break;
            case 'show':
                // Get
                snippet = ' @lrd:start\n' +
                    '   group={"${1:' + className + '}"},\n' +
                    '   summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
                break;
            case 'update':
                // Put
                snippet = ' @lrd:start\n' +
                    '   group={"${1:' + className + '}"},\n' +
                    '   summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
                break;
            case 'destroy':
                // Delete
                snippet = ' @lrd:start\n' +
                    '   group={"${1:' + className + '}"},\n' +
                    '   summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
                break;
            default:
                snippet = ' @lrd:start\n' +
                    '   group={"${1:' + className + '}"},\n' +
                    '   summary="${1} ' + actionName + '",\n' +
                    ' @lrd:end'
        }
        return snippet;
    }

    protected tags = {
        httpMethod: [
            {
                tag: '@OA\\Get(index)',
                snippet: 'index'
            },
            {
                tag: '@OA\\Get(show)',
                snippet: 'show'
            },
            {
                tag: '@OA\\Post(store)',
                snippet: 'store'
            },
            {
                tag: '@OA\\Put(update)',
                snippet: 'update'
            },
            {
                tag: '@OA\\Delete(destroy)',
                snippet: 'destroy'
            }
        ],
        types: [
            {
                tag: '@LRDparam',
                snippet: '@LRDparam ${1:key}'
            }
        ]
    }
}