// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import Completions from './completions';
import Indent from './indent';
import Decorator from './decorator';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	const provider = vscode.languages.registerCompletionItemProvider('php', new Completions(), '*', '@');
	context.subscriptions.push(provider);

	let decorator = new Decorator();
	let indent = new Indent();

	vscode.window.onDidChangeActiveTextEditor(editor => {
		indent.changeActiveTextEditor(editor);
		decorator.changeActiveTextEditor(editor);
	}, null, context.subscriptions);

	vscode.workspace.onDidChangeTextDocument(event => {
		indent.changeTextDocument(event);
		decorator.changeTextDocument(event);
	}, null, context.subscriptions);
}

// This method is called when your extension is deactivated
export function deactivate() {}
